package web;

import java.util.List;

import traitements.User;

import traitements.Reequest;

/**
 * @author elhamd
 *
 */
public class Model {
	private int id;
	private List<Reequest> requests;
	private User user;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public List<Reequest> getRequests() {
		return requests;
	}
	public void setRequests(List<Reequest> requests) {
		this.requests = requests;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	} 	
	

}
 