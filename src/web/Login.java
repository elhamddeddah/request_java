package web;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import traitements.ImplRequest;
import traitements.ImplUser;
import traitements.InterfaceRequest;
import traitements.InterfaceUser;
import traitements.User;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static String email;
	public static String password;
	public static InterfaceUser userInterface = new ImplUser();
	public static User user;
	public static InterfaceRequest requestInterface = new ImplRequest();
	public static Model model = new Model();

	/**
	 * @see HttpServlet#HttpServlet()
	 */

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		if (session.getAttribute("isAdmin") != null) {
			try {
				model.setRequests(requestInterface.showReequest());
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			request.setAttribute("model", model);

			this.getServletContext().getRequestDispatcher("/WEB-INF/request.jsp").forward(request, response);

		} else
			this.getServletContext().getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {

			email = request.getParameter("email");
			password = request.getParameter("pwd");
			user = userInterface.login(new User(password, email));
			if (user == null) {
				request.setAttribute("error",
						"Les donn�es entr�es ne correspondent<br/> � aucun user !");
				this.getServletContext().getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
			} else {
				System.out.println(user.getFirstname());
				HttpSession session = request.getSession();
				session.setAttribute("fullName", user.fullName());
				session.setAttribute("isAdmin", user.isAdmin());
				response.sendRedirect(request.getContextPath() + "/request");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
