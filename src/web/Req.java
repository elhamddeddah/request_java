package web;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import traitements.ImplRequest;
import traitements.ImplUser;
import traitements.InterfaceRequest;
import traitements.InterfaceUser;
import traitements.Reequest;
import traitements.User;

/**
 * Servlet implementation class Reequest
 */
@WebServlet("/Reequest")
public class Req extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public static InterfaceUser userInterface=new ImplUser();
    public static Reequest req; 
    public static InterfaceRequest requestInterface=new ImplRequest();
	public static Model model=Login.model;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Req() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		if (session.getAttribute("isAdmin") != null) {
				try {
					model.setRequests(requestInterface.showReequest());
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				request.setAttribute("model",model);
				
				this.getServletContext().getRequestDispatcher("/WEB-INF/request.jsp" ).forward( request, response );
				}
		else {
			request.setAttribute("error",
					"Connectez vous pour continuer !");
			this.getServletContext().getRequestDispatcher("/WEB-INF/index.jsp" ).forward( request, response );

			}
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int  id;
		try {
			String action = request.getParameter("action");
			switch (action) {
					case "add_req" :
						String object=request.getParameter("object");
						String detail=request.getParameter("detail");
						req=new Reequest(object,detail);
						requestInterface.addrequest(req);
					    break;
					case "approve_req" :
					    id = Integer.parseInt(request.getParameter("id"));
						requestInterface.approverequest(id);
						break;
					case "reject_req" :
					    id = Integer.parseInt(request.getParameter("id"));
					    requestInterface.rejectrequest(id);
						break;
			}
			response.sendRedirect(request.getContextPath() + "/request");

		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();}

	}

}
