package traitements;


public class User {

	private int id;
	private String password;
	private String role;
	private String email;
	private String firstname;
	private String lastname;
	/**
	 * @param id
	 * @param role
	 * @param email
	 * @param firstname
	 * @param lastname
	 */
	
	public User(int id, String role, String email, String firstname, String lastname) {
		super();
		this.id = id;
		this.role = role;
		this.email = email;
		this.firstname = firstname;
		this.lastname = lastname;
	}
	/**
	 * 
	 */
	public User() {
		super();
	}
	/**
	 * @param password
	 * @param email
	 */
	public User(String password, String email) {
		super();
		this.password = password;
		this.email = email;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String fullName() {
		return firstname.toUpperCase()+"  "+lastname.toUpperCase();
	}
	public Boolean isAdmin() {
		if(this.role.equalsIgnoreCase("admin"))
				return true;
		else 
			    return false;
	}
	


}
