package traitements;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ImplRequest implements InterfaceRequest {
	
	@Override
	public List<Reequest> showReequest() throws SQLException {
		// TODO Auto-generated method stub
		Connection conn = Connexion.getConnection();
		PreparedStatement ps = conn.prepareStatement("select * from request order by date desc;");
		ResultSet rs = (ResultSet) ps.executeQuery();
		List<Reequest> liste = new ArrayList<Reequest>();
		while (rs.next()) {
			Reequest r = new Reequest(rs.getInt("id"), rs.getString("object"), rs.getString("detail"),
					rs.getString("status"), rs.getDate("date"));
			liste.add(r);
		}
		return liste;

	}
	
	@Override
	public void addrequest(Reequest request) throws SQLException {
		// TODO Auto-generated method stub
		Connection conn = Connexion.getConnection();
		PreparedStatement ps = conn
				.prepareStatement("insert into request(object,detail,status,date) values(?,?,?,now())");
		ps.setString(1, request.getObject());
		ps.setString(2, request.getDetail());
		ps.setString(3, "en attente");
		//System.out.println(ps.getm);
		ps.executeUpdate();
	}

	@Override
	public void approverequest(int requestid) throws SQLException {
		// TODO Auto-generated method stub
		Connection conn = Connexion.getConnection();
		PreparedStatement ps = conn.prepareStatement("UPDATE request SET status = 'valid�' WHERE id =?");
		ps.setInt(1, requestid);
		ps.executeUpdate();

	}
	@Override
	public void rejectrequest(int requestid) throws SQLException {
		// TODO Auto-generated method stub
		Connection conn = Connexion.getConnection();
		PreparedStatement ps = conn.prepareStatement("UPDATE request SET status = 'r�jet�' WHERE id =?");
		ps.setInt(1, requestid);
		ps.executeUpdate();

	}


}
