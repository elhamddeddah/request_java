package traitements;

import java.util.Date;

public class Reequest {
	private int id;
	private String object;
	private String detail;
	private String status;
	private Date date;

	/**
	 * @param id
	 * @param object
	 * @param detail
	 * @param status
	 * @param date
	 */
	public Reequest(int id, String object, String detail, String status, Date date) {
		super();
		this.id = id;
		this.object = object;
		this.detail = detail;
		this.status = status;
		this.date = date;
	}

	/**
	 * @param object
	 * @param detail
	 */
	public Reequest(String object, String detail) {
		super();
		this.object = object;
		this.detail = detail;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getObject() {
		return object;
	}

	public void setObject(String object) {
		this.object = object;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
