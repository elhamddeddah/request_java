package traitements;

import java.sql.SQLException;
import java.util.List;


public interface InterfaceUser {
	User login(User user) throws SQLException;
	User logout(User user) throws SQLException;
   
}
