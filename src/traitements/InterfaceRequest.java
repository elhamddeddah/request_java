package traitements;

import java.sql.SQLException;
import java.util.List;

public interface InterfaceRequest {
	List<Reequest> showReequest() throws SQLException;
	void addrequest(Reequest r) throws SQLException;
	void approverequest(int requestid) throws SQLException;
	void rejectrequest(int requestid) throws SQLException;


}
