<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page
	import="java.util.List, java.util.ArrayList,traitements.Reequest,web.Model"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
<title>Request</title>
</head>
<body style="background-color: #F0FFFF">

	<style>
table {
	font-family: arial, sans-serif;
	border-collapse: collapse;
	width: 100%;
}

td, th {
	border: 1px solid #dddddd;
	text-align: left;
	padding: 8px;
}

tr:nth-child(even) {
	background-color: #eeee;
}
</style>
	<div style="width: 100%;">
		<form method="get" action="logout">
			<button type="submit" class="btn btn-dark float-md-right m-2">Déconnexion</button>
		</form>
		<h3 class="m-2">Bonjour ${ (sessionScope.fullName )}</h3>
	</div>

	<div class="container">
		<div class="row m-3">

			<div class="col-md-8">


				<h3 class="text-center">Liste des demandes</h3>
				<table>
					<tr>
						<th>#</th>
						<th>Objet</th>
						<th>Detail</th>
						<th>Date</th>
						<th>Status</th>
						<%
							if ((Boolean) session.getAttribute("isAdmin")) {
						%>
						<th>Action</th>
						<%
							}
						%>
					</tr>
					<%
						List<Reequest> requests = (ArrayList<Reequest>) ((Model) request.getAttribute("model")).getRequests();
					for (int i = 0; i < requests.size(); i++) {
					%>
					<tr>
						<td>
							<%
								out.println(i + 1);
							%>
						</td>
						<td>
							<%
								out.println(requests.get(i).getObject());
							%>
						</td>
						<td>
							<%
								out.println(requests.get(i).getDetail());
							%>
						</td>
						<td>
							<%
								out.println(requests.get(i).getDate());
							%>
						</td>
						<td>
							<%
								out.println(requests.get(i).getStatus());
							%>
						</td>
						<td>
							<%
								if ((Boolean) session.getAttribute("isAdmin") && requests.get(i).getStatus().equals("en attente")) {
							%>
							<div class="row  d-felex flex-justify-end">
								<div class="col-md-6">
									<form method="post" action="request"">
										<input type="text" name="action" hidden value="approve_req">
										<input type="text" name="id" hidden
											value="<%out.println(requests.get(i).getId());%>"> <span><button
												type="submit" class="btn btn-success btn-sm">Valider</button></span>
									</form>
								</div>
								<div class="col-md-6">
									<form method="post" action="request">
										<input type="text" name="action" hidden value="reject_req">
										<input type="text" name="id" hidden
											value="<%out.println(requests.get(i).getId());%>"> <span><button
												type="submit" class="btn btn-danger btn-sm ">Réjeter</button></span>
									</form>
								</div>
							</div> <%
 	}
 %>
						</td>

					</tr>
					<%
						}
					%>
				</table>
			</div>
			<div class="col-md-4">


				<form method="post" action="request" class="m-3">
					<input type="text" name="action" hidden value="add_req">
					<h3 class="text-center">Ajouter votre demande</h3>
					<h5 style="color: red;">${error}</h5>
					<label>Objet : </label> <br /> <input type="text" name="object"
						placeholder="Objet" required><br /> <label>Détail
						:</label><br />
					<textarea rows="6" cols="25" name="detail" placeholder="Detail"
						required> </textarea>
					<br />
					<button type="submit" class="btn btn-primary">Envoyer</button>
				</form>
			</div>
		</div>
	</div>

</body>
</html>