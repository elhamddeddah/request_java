<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
<style type="text/css">
.centre{
	background-color: #007bff;
	height: 350px;
	width: 500px;
	margin: auto;
	margin-top: 160px ;
	
	padding:20px;
	
}</style>
<link rel="stylesheet" href="css/style.css">
<title></title>
</head>
<body style="background-color: #ECF3FF;">

		<div class="centre" class="align-self-center">
		<h3 class="text-white">Connexion:</h3>
		<strong class=" text-danger"> ${error}</strong>
		  <div class="formcolor">
		<form  method="post" action="login">
			 <div class="form-group">
			    <label for="exampleInputEmail1" class="text-white">Email : </label>
			    <input type="text" class="form-control" name="email"  placeholder="Username">
			    
			  </div>
			  <div class="form-group">
			    <label for="exampleInputPassword1" class="text-white">Password :</label>
			    <input type="password" class="form-control" name="pwd" placeholder="Password">
			  </div>
			  
			  
			  <button class="btn btn-success form-control" type="submit" >
			  <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
			  <span >Login</span>
			</button>
		</form>
		</div>
		
		 </div>
		 
</body>
