 

  ## Table of contents
* [Description](#DESCRIPTION)
* [Technologies](#technologies)
* [Setup](#setup)

## Description
#### This project is WEB APP written in java EE (Servelet and jsp)
  #### allowing users to:
  * Authenticate
  * launch their requests by entering the subject, description, comment etc...
  #### allowing administrators to:
  * Authenticate
  * validate or reject requests by changing the status of
  requests.
	
## Technologies
### This project is created with:
* JAVA EE

	
## Setup


To run this project open it locally with Eclipse or Intellij or other and add: 
* jdbc library (i had mysql-connector-java-8.0.11.jar)
* Tomcat serve  (Apache Tomcat v9.0)
* mysql and create db named request  and run : 

```
  CREATE TABLE `user` (
 `id` int NOT NULL AUTO_INCREMENT,
  `firstname` varchar(15) DEFAULT NULL,
  `lastname` varchar(15) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `password` varchar(30) DEFAULT NULL,
  `role` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1;
--
-- Dumping data for table `user`
--

INSERT INTO `user` VALUES (1,'firstname0','lastname0','firstname0@gmail.com','123456toHash','admin'),(2,'firstname1','lastname1','firstname1@gmail.com','123456toHash','user');


CREATE TABLE `request` (
  `id` int NOT NULL AUTO_INCREMENT,
  `object` varchar(15) DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL,
  `detail` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 ;


--
-- Dumping data for table `request`
--


INSERT INTO `request` VALUES (1,'sur carte','réjeté','blabla ','2021-02-14'),(2,'sur carte','validé','blabla ','2021-02-14'),(3,'sur carte','réjeté','blabla ','2021-02-14'),(4,'sur carte','réjeté','blabla ','2021-02-14'),(5,'sur carte','validé','blabla ','2021-02-14'),(6,'sur carte','en attente','blabla ','2021-02-14'),(7,'hh','validé','lll','2021-02-16'),(8,'aaaaa','réjeté',' 455','2021-02-16'),(9,'aaaaa','en attente',' 455','2021-02-16'),(10,'aaaaa','en attente',' 455','2021-02-16'),(11,'aaaaa','en attente',' 455','2021-02-16'),(12,'aaaaa','en attente',' 455','2021-02-16'),(13,'aaaaa','en attente',' 455','2021-02-16'),(14,'aaaaa','validé',' 455','2021-02-16'),(15,'a5','réjeté',' a5 dttttttt','2021-02-17'),(16,'a56666','validé',' yufhfhffh','2021-02-17'),(17,'aaaaa','en attente','mklmkmkl','2021-02-17');

```
